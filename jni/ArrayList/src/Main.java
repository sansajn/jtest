import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        System.out.println(playlist() + " and " + indices());
    }

    private static native ArrayList<String> playlist();
    private static native ArrayList<Integer> indices();

    static {
        System.loadLibrary("Main");
    }
}
