#include <vector>
#include <string>
#include <iostream>
#include "Main.h"

using std::string;
using std::vector;
using std::cout;


jobject to_ArrayList(JNIEnv * env, vector<string> const & v) 
{
	jclass java_util_ArrayList = env->FindClass("java/util/ArrayList");
	jmethodID java_util_ArrayList_ = env->GetMethodID(java_util_ArrayList, "<init>", "(I)V");
	jmethodID java_util_ArrayList_size = env->GetMethodID(java_util_ArrayList, "size", "()I");
	jmethodID java_util_ArrayList_get = env->GetMethodID(java_util_ArrayList, "get", "(I)Ljava/lang/Object;");
	jmethodID java_util_ArrayList_add = env->GetMethodID(java_util_ArrayList, "add", "(Ljava/lang/Object;)Z");
	
	jobject result = env->NewObject(java_util_ArrayList, java_util_ArrayList_, v.size());
	for (string const & s : v) 
	{
		jstring element = env->NewStringUTF(s.c_str());
		env->CallBooleanMethod(result, java_util_ArrayList_add, element);
		env->DeleteLocalRef(element);
	}
  
	return result;
}

jobject to_ArrayList(JNIEnv * env, vector<int> const & v) 
{
	jclass java_util_ArrayList = env->FindClass("java/util/ArrayList");
	jmethodID java_util_ArrayList_ = env->GetMethodID(java_util_ArrayList, "<init>", "(I)V");
	jmethodID java_util_ArrayList_add = env->GetMethodID(java_util_ArrayList, "add", "(Ljava/lang/Object;)Z");
	jclass java_lang_Integer = env->FindClass("java/lang/Integer");
	jmethodID java_lang_Integer_ = env->GetMethodID(java_lang_Integer, "<init>", "(I)V");
	
	jobject result = env->NewObject(java_util_ArrayList, java_util_ArrayList_, v.size());
	for (int const & n : v) 
	{
		jobject element = env->NewObject(java_lang_Integer, java_lang_Integer_, n);
		env->CallBooleanMethod(result, java_util_ArrayList_add, element);
		env->DeleteLocalRef(element);
	}
  
	return result;
}

JNIEXPORT jobject JNICALL Java_Main_playlist
  (JNIEnv * env, jclass)
{
	return to_ArrayList(env, {"one", "two", "three"});
}

JNIEXPORT jobject JNICALL Java_Main_indices
  (JNIEnv * env, jclass)
{
	return to_ArrayList(env, {1, 2, 3});
}
