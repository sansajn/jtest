import java.util.ArrayList;

public class Main {
	public static void main(String[] args) {}

	public native void returnVoid();
	public native Integer returnInteger();
	public native String returnString();
	public native ArrayList<String> returnStringList();
	public native ArrayList<Integer> returnIntegerList();
	public native void callLong(long value);
	public native void callString(String s);
	public native void callArrayListString(ArrayList<String> items);
}
