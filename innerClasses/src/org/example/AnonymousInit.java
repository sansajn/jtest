package org.example;

// An anonymous inner class that performs initialization. (innerclasses/Parcel9.java)
public class AnonymousInit {

	// Argument must be final to use inside anonymous inner class.
	public Destination dest(final String dest) {
		return new Destination() {
			private String label = dest;
			public String readLabel() {
				return label;
			}
		};
	}

	public static void main(String[] args) {
		AnonymousInit p = new AnonymousInit();
		Destination d = p.dest("Tasmania");
	}
}
