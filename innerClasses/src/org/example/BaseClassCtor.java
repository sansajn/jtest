package org.example;

// Calling the base-class constructor (innerclasses/Parcel8.java)
public class BaseClassCtor {

	public Wrapping wrapping(int x) {
		return new Wrapping(x) {
			public int value() {
				return super.value() * 47;
			}
		};
	}

	public static void main(String[] args) {
		BaseClassCtor p = new BaseClassCtor();
		Wrapping w = p.wrapping(10);
		System.out.println("w.value:" + w.value());
	}
}
