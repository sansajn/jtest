package org.example;

public interface Destination {
	String readLabel();
}
